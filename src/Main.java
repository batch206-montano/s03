import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {


        String[] topGames ={"Pokemon Sword", "Pokemon Shield", "Super Smash Bros. Ultimate", "Luigi's Mansion 3 ","Mario Odyssey"};

        HashMap<String,Integer> allGames = new HashMap<>();
        allGames.put(topGames[0], 30);
        allGames.put(topGames[1], 100);
        allGames.put(topGames[2],20);
        allGames.put(topGames[3], 15);
        allGames.put(topGames[4], 50);

        allGames.forEach((key,value) -> {
            System.out.println( key +" has "+ value + " stocks left");
        });

        allGames.forEach((key,value) -> {

            if (value <= 30) {
                System.out.println(key + " has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games:");

        String[] allTopGames = new String[3];

        allTopGames[0] = "Pokemon Sword";
        allTopGames[1] = "Super Smash Bro.";
        allTopGames[2] = "Luigi's Mansion 3";
        System.out.println(Arrays.deepToString(allTopGames));
        boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No?");
            Scanner userInput = new Scanner(System.in);
            String input = userInput.nextLine();
            if(input.equals("Yes")){
                System.out.println("Add Item Name");
                String itemName = userInput.nextLine();
                System.out.println("Add Number of Stocks");
                int itemStock = userInput.nextInt();
                gameStocks.put(itemName, itemStock);
                addItem = false;
                break;
            } else if(input.equals("No")){
                addItem = false;
                System.out.println("Thank you");
                break;
            } else {
                System.out.println("Invalid input. Try again.");
            }
//            switch (input) {
//                case "Yes":
//                    System.out.println("Add Item Name");
//                    String itemName = userInput.nextLine();
//                    System.out.println("Add Number of Stocks");
//                    int itemStock = userInput.nextInt();
//                    gameStocks.put(itemName, itemStock);
//                    addItem = false;
//                    break;
//                case "No":
//                    addItem = false;
//                    System.out.println("Thank you");
//                    break;
//                default:
//                    System.out.println("Invalid input. Try again.");
//            }
        }

    }
}
